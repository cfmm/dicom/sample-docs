"""
sample_docs
-------------

This is the description for that library
"""
from setuptools import setup, find_packages


setup(
    name="sample_docs",
    version="1.0",
    url="https://gitlab.com/cfmm/dicom/samdple-docs/",
    license="MIT",
    author="Martyn Klassen",
    author_email="lmklassen@gmail.com",
    description="Sample documentation extension for Flask-Admin",
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms="any",
    install_requires=[
        "Flask>=2.0.1",
        "Flask-Admin>=1.5.8",
    ],
    classifiers=[
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
