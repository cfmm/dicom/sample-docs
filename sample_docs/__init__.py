import typing as t

from flask_babel import gettext

from .views import DocumentationView, create_blueprint

if t.TYPE_CHECKING:
    from flask_admin import Admin
    from flask import Flask


class Docs(object):
    def __init__(
        self, app: "Flask" = None, admin: "Admin" = None, name: str = None
    ) -> None:
        self.app = app
        self.name = name or "docs"
        self.admin = admin
        self.config = dict()
        if app is not None and admin is not None:
            self.init_app(app, admin)

    def init_app(
        self, app: "Flask", admin: "Admin", name: str = None, config: dict = None
    ) -> None:
        self.config = config or self.config
        self.name = name or self.name
        public = self.config.get("public", [])

        create_blueprint(app, name=self.name, static_url_path=admin.static_url_path)
        admin.add_view(
            DocumentationView(
                name=gettext("Help"),
                endpoint=f"{self.name}",
                url=f"/{self.name}",
                static_folder="static",
                public_pages=public,
            )
        )
