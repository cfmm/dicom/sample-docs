from flask import redirect, abort, url_for, Flask, Blueprint
from flask_admin import BaseView, expose
from flask_login import current_user
from jinja2 import TemplateNotFound


def create_blueprint(app: Flask, name: str, static_url_path: str = None) -> None:
    bp = Blueprint(
        "sample_docs",
        __name__,
        template_folder="templates",
        static_folder="static",
        static_url_path=static_url_path or f"/{name}/static",
    )
    app.register_blueprint(bp)


class DocumentationView(BaseView):
    def __init__(self, *args, public_pages=None, **kwargs):
        try:
            self.public_pages = [page.lower() for page in public_pages]
        except TypeError:
            self.public_pages = []

        super().__init__(*args, **kwargs)

    # noinspection PyMethodMayBeStatic
    def is_visible(self):
        return False

    @expose("/")
    def index(self):
        return self.page("index")

    @expose("/<page>")
    def page(self, page):
        page = page.lower()

        def render_with_context():
            if current_user.is_authenticated:
                try:
                    is_admin = bool(current_user.is_datacare())
                    is_datacare = bool(current_user.is_datacare())
                except AttributeError:
                    is_admin = False
                    is_datacare = False

                docs = {
                    "is_admin": is_admin,
                    "is_datacare": is_datacare,
                    "is_user": not is_datacare,
                }
            else:
                docs = {
                    "is_admin": False,
                    "is_datacare": False,
                    "is_user": False,
                }
            docs["is_authenticated"] = current_user.is_authenticated

            try:
                return self.render(
                    "sample_docs/{page}.html",
                )
            except TemplateNotFound:
                abort(404)

        # Display HTML-rendered documentation page if user is logged in
        if current_user.is_authenticated:
            return render_with_context()
        else:
            if page in self.public_pages:
                return render_with_context()
            return redirect(
                url_for(
                    "security.login", next=url_for(f"{self.endpoint}.page", page=page)
                )
            )
